﻿using System.Collections.Generic;
using System.Windows;

namespace Oefening_28._5___Printers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        PC pc = new PC();
        Printer printer1 = new Printer("Printer 1");
        Printer printer2 = new Printer("Printer 2");
        Printer printer3 = new Printer("Printer 3");
        Printer printer4 = new Printer("Printer 4");

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            imgPrinterBezet1.Visibility = Visibility.Hidden;
            imgPrinterBezet2.Visibility = Visibility.Hidden;
            imgPrinterBezet3.Visibility = Visibility.Hidden;
            imgPrinterBezet4.Visibility = Visibility.Hidden;
            pc.AddPrinter(printer1);
            pc.AddPrinter(printer2);
            pc.AddPrinter(printer3);
            pc.AddPrinter(printer4);
            lblPrinter1.Content = printer1.ToString();
            lblPrinter2.Content = printer2.ToString();
            lblPrinter3.Content = printer3.ToString();
            lblPrinter4.Content = printer4.ToString();
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            int teller = 0;
            foreach (Printer printer in pc.Printers)
            {
                teller++;
                if (printer.Busy == false)
                {
                    if (teller  == 1)
                    {
                        imgPrinterBezet1.Visibility = Visibility.Visible;
                    }
                    if (teller == 2)
                    {
                        imgPrinterBezet2.Visibility = Visibility.Visible;
                    }
                    if (teller == 3)
                    {
                        imgPrinterBezet3.Visibility = Visibility.Visible;
                    }
                    if (teller == 4)
                    {
                        imgPrinterBezet4.Visibility = Visibility.Visible;
                    }
                    printer.Busy = pc.PrintAf();
                    printer.ToString();
                    lblPrinter1.Content = printer1.ToString();
                    lblPrinter2.Content = printer2.ToString();
                    lblPrinter3.Content = printer3.ToString();
                    lblPrinter4.Content = printer4.ToString();
                    break;
                }
                
            }
            teller = 0;
            foreach (Printer printerBezet in pc.Printers)
            {
                if (printerBezet.Busy == true)
                {
                    teller++;
                }
                if (teller == pc.Printers.Count)
                {
                    MessageBox.Show("Alle printers zijn bezet, gelieve even te wachten", "Bezet", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void btnResetPrinter1_Click(object sender, RoutedEventArgs e)
        {
            printer1.Reset();
            lblPrinter1.Content = printer1.ToString();
            imgPrinterBezet1.Visibility = Visibility.Hidden;
        }

        private void btnResetPrinter2_Click(object sender, RoutedEventArgs e)
        {
            printer2.Reset();
            lblPrinter2.Content = printer2.ToString();
            imgPrinterBezet2.Visibility = Visibility.Hidden;
        }

        private void btnResetPrinter3_Click(object sender, RoutedEventArgs e)
        {
            printer3.Reset();
            lblPrinter3.Content = printer3.ToString();
            imgPrinterBezet3.Visibility = Visibility.Hidden;
        }

        private void btnResetPrinter4_Click(object sender, RoutedEventArgs e)
        {
            printer4.Reset();
            lblPrinter4.Content = printer4.ToString();
            imgPrinterBezet4.Visibility = Visibility.Hidden;
        }
    }
}
