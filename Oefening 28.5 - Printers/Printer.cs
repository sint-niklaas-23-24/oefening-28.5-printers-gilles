﻿namespace Oefening_28._5___Printers
{
    class Printer
    {
        private bool _busy;
        private string _naam;

        public Printer() { }
        public Printer(string naam)
        {
            this.Naam = naam;
        }

        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public bool Busy
        {
            get { return _busy; }
            set { _busy = value; }
        }

        public void Reset()
        {
            this.Busy = false;
        }
        public override string ToString()
        {
            string resultaat;
            if (Busy == false)
            {
                resultaat = $"{Naam}: wachtende op een opdracht";
            }
            else
            {
                resultaat = $"{Naam}: bezig met een printopdracht";
            }
            return resultaat;
        }
    }
}
